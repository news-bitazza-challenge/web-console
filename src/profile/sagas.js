import { all, take, call, put } from 'redux-saga/effects'
import api from '../common/utils/web-api'
import localState from '../common/utils/local-state'
import * as profileTypes from './types'


function* logout() {
  while (true) {
    yield take(profileTypes.LOGOUT_REQ)
    yield call(api.logout)
    localState.clean()
    window.location.href = '/'
  }
}


export default function* () {
  yield all([
    logout()
  ])
}