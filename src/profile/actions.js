import { bindActionCreators } from 'redux'
import * as profileTypes from './types'

let actions = {}

actions.logout = () => ({
	type: profileTypes.LOGOUT_REQ,
	payload: null,
})

export default (dispatch) => ({ ...bindActionCreators(actions, dispatch)})