import React from 'react'
import { MuiThemeProvider } from '@material-ui/core/styles'
import DateFnsUtils  from '@date-io/date-fns'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import { Provider } from 'react-redux'
import theme from './theme'
import initStore from './common/redux/store'
import Loading from './common/components/loading'
import Login from './common/components/login'
import RootContainer from './common/components/root-container'


const App = ({ initState }) => {
  const store = initStore(initState)
  return (
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <RootContainer />
        </MuiPickersUtilsProvider>
      </Provider>
    </MuiThemeProvider>
  )
}

const LoginApp = () => (
  <MuiThemeProvider theme={theme}>
    <Login />
  </MuiThemeProvider>
)

const LoadingApp = () => (
  <MuiThemeProvider theme={theme}>
    <Loading />
  </MuiThemeProvider>
)

export { LoadingApp, LoginApp }

export default App
