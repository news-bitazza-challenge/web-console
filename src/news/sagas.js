import { all, take, call, put, select } from 'redux-saga/effects'
import api from '../common/utils/web-api'
import * as newsTypes from './types'


function* fetchAllNews() {
  while (true) {
    yield take(newsTypes.FETCH_ALL_NEWS_REQ)
    const store = yield select()
    const res = yield call(api.fetchAllNews, store.news.allNewsDateFilter)
    yield put({
      type: newsTypes.UPDATE_ALL_NEWS,
      payload: res.data,
    })
  }
}

function* updateAllNewsDateFilter() {
  while (true) {
    yield take(newsTypes.UPDATE_ALL_NEWS_DATE_FILTER)
    yield put({ type: newsTypes.FETCH_ALL_NEWS_REQ })
  }
}

function* fetchNews() {
  while (true) {
    const action = yield take(newsTypes.FETCH_NEWS_REQ)
    const res = yield call(api.fetchNews, action.payload.slug)
    yield put({
      type: newsTypes.UPDATE_NEWS,
      payload: res.data,
    })
  }
}


function* deleteNews() {
  while (true) {
    const action = yield take(newsTypes.DELETE_NEWS_REQ)
    yield call(api.deleteNews, action.payload.slug)
    yield put({ type: newsTypes.FETCH_ALL_NEWS_REQ })
    yield put({ type: newsTypes.UPDATE_NEWS, payload: {
      slug: '',
      title: '',
      description: '',
      publish: '',
      image: '',
    } })
  }
}

export default function* () {
  yield all([
    fetchAllNews(),
    updateAllNewsDateFilter(),
    fetchNews(),
    deleteNews(),
  ])
}