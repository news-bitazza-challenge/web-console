import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { KeyboardDatePicker } from '@material-ui/pickers'
import newsActions from '../actions'
import NewsDetail from './news-detail'
import AddNews from './add-news'


const useStyles = makeStyles(theme => ({
	filterBox: {
		borderBottom: `1px solid ${theme.palette.grey['200']}`,
		display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
	},
	list: {
		padding: theme.spacing(0),
		borderRight: `1px solid ${theme.palette.grey['200']}`,
	},
	listItem: {
		borderTop: `1px solid ${theme.palette.grey['200']}`,
	},
	newsDetailMobile: {
		display: 'block',
		[theme.breakpoints.up('md')]: {
			display: 'none',
		},
	},
	newsDetailDesktop: {
		display: 'none',
		[theme.breakpoints.up('md')]: {
			display: 'block',
		},
	}
}))

const NewsContainer = (props) => {
	const classes = useStyles()
	const [filterDate, setFilterDate] = React.useState(null)
	const [newsSlug, setNewsSlug] = React.useState('')
	const { allNews } = props

	const handleFetchNews = (slug) => {
		props.newsActions.fetchNews(slug)
		setNewsSlug(slug)
	}

	const handleChangeFilterDate = (date) => {
		setFilterDate(date)
		props.newsActions.updateAllNewsDateFilter(date)
	}

	React.useEffect(() => {
		props.newsActions.fetchAllNews()
	}, [])
	
  return (
		<Paper className={classes.root}>
			<Box className={classes.filterBox} pl={2} pr={2}>
				<KeyboardDatePicker
					disableToolbar
					variant="inline"
					format="MM/dd/yyyy"
					margin="normal"
					label="Filter Date"
					value={filterDate}
					onChange={handleChangeFilterDate}
					KeyboardButtonProps={{
						'aria-label': 'change date',
					}}/>
					<AddNews />
			</Box>
			<Grid container>
				<Grid item xs={12} md={4}>
					<List className={classes.list} component="nav">
						{allNews.map(news => (
							<Box key={news.slug}>
								<ListItem
									selected={news.slug === newsSlug}
									className={classes.listItem}
									onClick={(e) => handleFetchNews(news.slug)}
									button>
									<ListItemText
										primary={news.title}
										secondary={news.publish.toLocaleString()} />
								</ListItem>
								{news.slug === newsSlug && 
									<Box className={classes.newsDetailMobile}>
										<NewsDetail />
									</Box>
								}
							</Box>
						))}
					</List>
				</Grid>
				<Grid item md={8}>
					<Box className={classes.newsDetailDesktop}>
						<NewsDetail />
					</Box>
				</Grid>
			</Grid>
		</Paper>
	)
}

const mapStateToProps = (state) => ({
  allNews: state.news.allNews,
})

const mapDispatchToProps = (dispatch) => ({
  newsActions: newsActions(dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewsContainer)
