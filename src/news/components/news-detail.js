import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import { KeyboardDatePicker } from '@material-ui/pickers'
import api from '../../common/utils/web-api'
import newsActions from '../actions'


const useStyles = makeStyles(theme => ({
	container: {
		padding: theme.spacing(5),
	},
	img: {
		width: '100%',
	},
}))

const NewsDetail = (props) => {
	const classes = useStyles()
	const [title, setTitle] = React.useState('')
	const [description, setDescription] = React.useState('')
	const [publish, setPublish] = React.useState(null)
	const [image, setImage] = React.useState('')
	const [fileFormData, setFileFormData] = React.useState('')
	const [resMess, setResMess] = React.useState('')

	React.useEffect(() => {
		setTitle(props.news.title)
		setDescription(props.news.description)
		setPublish(props.news.publish)
		setImage(props.news.image)
		setResMess('')
	}, [props.news])

	const handleUploadImage = (input) => {
		const reader = new FileReader()
		reader.onload = (e) => setImage(e.target.result)
		reader.readAsDataURL(input.target.files[0])
		
		const formData = new FormData()
		formData.append('image', input.target.files[0])
		setFileFormData(formData)
	}

	const handleDeleteNews = () => {
		props.newsActions.deleteNews(props.news.slug)
	}

	const handleUpdateNews = async () => {
		let fileUpdateUrl
		if (fileFormData) {
			const fileUploadRes = await api.upload(fileFormData)
			fileUpdateUrl = fileUploadRes.data.url
		}
		await api.updateNews(props.news.slug, {
			title,
			description,
			publish,
			image: fileUpdateUrl || image,
		})
		setResMess('Successful update')
		setTimeout(() => setResMess(''), 3000)
	}

	if (!props.news.slug) {
		return null
	}
	
  return (
		<Grid className={classes.container} container spacing={3}>
			<Grid item xs={12}>
				<TextField
					label='Slug'
					type='text'
					value={props.news.slug}
					disabled={true}
					fullWidth/>
			</Grid>
			<Grid item xs={12}>
				<TextField
					label='Title'
					type='text'
					value={title}
					onChange={(e) => setTitle(e.target.value)}
					multiline
					fullWidth/>
			</Grid>
			<Grid item xs={12}>
				<TextField
					label='Description'
					type='text'
					value={description}
					onChange={(e) => setDescription(e.target.value)}
					multiline
					fullWidth/>
			</Grid>
			<Grid item xs={12}>
				<KeyboardDatePicker
					label='Publish'
					disableToolbar
					variant='inline'
					format='dd/MM/yyyy'
					value={publish}
					onChange={date => setPublish(date)}
					KeyboardButtonProps={{
						'aria-label': 'change date',
					}}
					fullWidth/>
			</Grid>
			<Grid item xs={12}>
				<input type='file' onChange={handleUploadImage} />
			</Grid>
			{image && 
				<Grid item xs={12}>
					<img className={classes.img} src={image}/>
				</Grid>
			}
			{resMess && 
				<Grid item xs={12}>
					<Typography
						align="center"
						variant="body1"
						color="textSecondary" >
						{resMess}
					</Typography>
				</Grid>
			}
			<Grid item xs={6}>
				<Button
					onClick={handleDeleteNews}
					variant='outlined'
					color='primary'
					fullWidth>
					Delete
				</Button>
			</Grid>
			<Grid item xs={6}>
				<Button
					onClick={handleUpdateNews}
					variant='contained'
					color='primary'
					fullWidth>
					Save
				</Button>
			</Grid>
		</Grid>
	)
}

const mapStateToProps = (state) => ({
  news: state.news.news,
})

const mapDispatchToProps = (dispatch) => ({
  newsActions: newsActions(dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewsDetail)