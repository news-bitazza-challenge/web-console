import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Tooltip from '@material-ui/core/Tooltip'
import Fab from '@material-ui/core/Fab'
import PostAddIcon from '@material-ui/icons/PostAdd'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import { CommonDialog } from '../../common/components/block/dialog'
import api from '../../common/utils/web-api'


const useStyles = makeStyles(theme => ({
	img: {
		width: '100%',
	},
}))

const CreateNews = () => {
	const classes = useStyles()
  const [open, setOpen] = React.useState(false)
  const [title, setTitle] = React.useState('')
  const [description, setDescription] = React.useState('')
	const [image, setImage] = React.useState('')
  const [fileFormData, setFileFormData] = React.useState('')
  const [resMess, setResMess] = React.useState('')

  const handleClickOpen = () => {
    setTitle('')
    setDescription('')
    setImage('')
    setFileFormData('')
    setOpen(true)
  }
  const handleClose = () => setOpen(false)

	const handleUploadImage = (input) => {
		const reader = new FileReader()
		reader.onload = (e) => setImage(e.target.result)
		reader.readAsDataURL(input.target.files[0])

		const formData = new FormData()
		formData.append('image', input.target.files[0])
		setFileFormData(formData)
	}

  const onCreateNews = async () => {
    const fileUploadRes = await api.upload(fileFormData)
    const fileUpdateUrl = fileUploadRes.data.url
    await api.addNews({
      title,
      description,
      image: fileUpdateUrl,
    })
    setTitle('')
    setDescription('')
    setImage('')
    setFileFormData('')
    setResMess('Successful created news')
    setTimeout(() => setResMess(''), 3000)
  }

  return (
    <Box >
      <Tooltip title="Create a news" placement="left">
        <Fab 
          aria-label="add"
          size="small"
          color="secondary"
          onClick={handleClickOpen}>
          <PostAddIcon />
        </Fab>
      </Tooltip>
      <CommonDialog
        title="Create a news"
        open={open}
        handleClose={handleClose}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              autoFocus
              label="Title"
              type="text"
              value={title}
              onChange={e => setTitle(e.target.value)}
              fullWidth/>
          </Grid>
          <Grid item xs={12}>
            <TextField
              label="Description"
              type="text"
              value={description}
              onChange={e => setDescription(e.target.value)}
              multiline
              fullWidth/>
          </Grid>
          <Grid item xs={12}>
            <input type='file' onChange={handleUploadImage} />
          </Grid>
          {image &&
            <Grid item xs={12}>
              <img className={classes.img} src={image}/>
            </Grid>
          }
          {resMess && 
            <Grid item xs={12}>
              <Typography
                align="center"
                variant="body1"
                color="textSecondary" >
                {resMess}
              </Typography>
            </Grid>
          }
          <Grid item xs={12}>
            <Button
              onClick={onCreateNews}
              color="primary"
              variant="contained"
              fullWidth>
              Sunmit
            </Button>
          </Grid>
        </Grid>
      </CommonDialog> 
    </Box>
  )
}

export default CreateNews
