import * as newsTypes from './types'

/*
{
  allNews: [{
    title: string,
    slug: string,
    description: string,
    image: string,
    publish: string,
  }],
  allNewsDateFilter: Date,
  news: {
    title: string,
    slug: string,
    description: string,
    image: string,
    publish: string,
  }
}
*/
const initState = {
  allNews: [],
  allNewsDateFilter: null,
  news: {},
}

export default (state = initState, action) => {
  switch (action.type) {
    case newsTypes.UPDATE_ALL_NEWS:
      return {
        ...state,
        allNews: action.payload.map(news => ({...news, publish: new Date(news.publish)})),
      }
    case newsTypes.UPDATE_ALL_NEWS_DATE_FILTER:
      return {
        ...state,
        allNewsDateFilter: action.payload.date,
      }
    case newsTypes.UPDATE_NEWS:
        return {
          ...state,
          news: { ...action.payload, publish: new Date(action.payload.publish) }
        }
    default:
      return state
  }
}