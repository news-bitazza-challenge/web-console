import { bindActionCreators } from 'redux'
import * as newsTypes from './types'

let actions = {}

actions.fetchAllNews = (date) => ({
    type: newsTypes.FETCH_ALL_NEWS_REQ,
    payload: { date },
})

actions.updateAllNewsDateFilter = (date) => ({
    type: newsTypes.UPDATE_ALL_NEWS_DATE_FILTER,
    payload: { date },
})

actions.fetchNews = (slug) => ({
    type: newsTypes.FETCH_NEWS_REQ,
    payload: { slug },
})

actions.deleteNews = (slug) => ({
    type: newsTypes.DELETE_NEWS_REQ,
    payload: { slug },
})


export default (dispatch) => ({ ...bindActionCreators(actions, dispatch)})