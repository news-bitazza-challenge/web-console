

export const FETCH_ALL_NEWS_REQ = 'news/FETCH_ALL_NEWS_REQ'
export const UPDATE_ALL_NEWS = 'news/UPDATE_ALL_NEWS'
export const UPDATE_ALL_NEWS_DATE_FILTER = 'news/UPDATE_ALL_NEWS_DATE_FILTER'

export const FETCH_NEWS_REQ = 'news/FETCH_NEWS_REQ'
export const UPDATE_NEWS = 'news/UPDATE_NEWS'
export const DELETE_NEWS_REQ = 'news/DELETE_NEWS_REQ'