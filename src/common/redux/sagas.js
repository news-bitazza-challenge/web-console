import { all } from 'redux-saga/effects'
import newsSage from '../../news/sagas'
import profileSage from '../../profile/sagas'



export default function* rootSage() {
  yield all([
    newsSage(),
    profileSage(),
  ])
}
