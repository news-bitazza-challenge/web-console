import { combineReducers } from 'redux'
import newsReducer from '../../news/reducers'
import profileReducer from '../../profile/reducers'


export default combineReducers({
  news: newsReducer,
  profile: profileReducer,
})