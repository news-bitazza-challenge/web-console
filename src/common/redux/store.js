import { applyMiddleware, createStore } from 'redux'
import { createLogger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import reducers from './reducers'
import rootSaga from './sagas'


const logger = createLogger({
  collapsed: true
})

const sagaMiddleware = createSagaMiddleware()

const middlewar = [
  logger,
  sagaMiddleware,
]

const initStore = (state) => {
  const store = createStore(
    reducers,
    { profile: state },
    applyMiddleware(...middlewar)
  )
  sagaMiddleware.run(rootSaga)
  return store
}

export default initStore