import axios from 'axios'
import localState from './local-state'
import { pickBy } from 'lodash'


const bearerFormat = (accessToken) => `Bearer ${accessToken}`

const webAPI = () => {
  const server = axios.create()
  const accessToken = localState.load().accessToken
  if(accessToken) {
    server.defaults.headers.common['Authorization'] = bearerFormat(accessToken)
  }
  server.defaults.headers.common['Content-Type'] = 'application/json'
  server.interceptors.response.use(
    response => ({ ...response, err: false }),
    error => {
      if(error.response.status === 401) {
        return server.get('/api/common/refresh-access-token', {
          headers: { refreshtoken: localState.load().refreshToken }
        }).then(res => {
          localState.save(res.data)
          error.config.headers['Authorization'] = bearerFormat(res.data.accessToken)
          server.defaults.headers.common['Authorization'] = bearerFormat(res.data.accessToken)
          return server.request(error.config)
        })
      }
      return { ...error.response, err: true }
    },
  )

  return {
    login: async (username, password) => {
      const res = await server.post('/api/common/sign-in', { username, password })
      return res
    },
    logout: async () => {
      const headers = { refreshtoken: localState.load().refreshToken }
      const res = await server.delete('/api/common/logout', { headers })
      return res
    },
    profile: async () => {
      const res = await server.get('/api/common/profile')
      return res
    },
    upload: async (formData) => {
      const headers = { 'Content-Type': 'multipart/form-data' }
      const res = await server.post('/api/common/upload', formData, { headers })
      return res
    },
    fetchAllNews: async (date) => {
      const params = pickBy({ date })
      const res = await server.get('/api/news/news', { params })
      return res
    },
    addNews: async (data) => {
      const res = await server.post('/api/news/news', data)
      return res
    },
    fetchNews: async (slug) => {
      const res = await server.get(`/api/news/news/${slug}`)
      return res
    },
    updateNews: async (slug, data) => {
      const res = await server.patch(`/api/news/news/${slug}`, data)
      return res
    },
    deleteNews: async (slug) => {
      const res = await server.delete(`/api/news/news/${slug}`)
      return res
    },
  }
}

export default webAPI()
